package dao;

import dto.Employee;
import dto.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EmployeeDao implements CrudDao<Employee> {
    @Autowired
    private JdbcTemplate template;

    public void create(Employee object) {
        template.update("INSERT INTO employees (employee_id, employee_name, position_id, salary) VALUES (?, ?, ?, ?)", object.getId(), object.getName(), object.getPosition().getId(), object.getSalary());
    }

    public Employee findById(Integer id) {
        return template.queryForObject("SELECT * FROM employees LEFT JOIN positions ON employees.position_id = positions.position_id WHERE employee_id=?", new Object[]{id},
                new EmployeeMapper());
    }

    public List<Employee> findAll() {
        return template.query("SELECT * FROM employees LEFT JOIN positions ON employees.position_id = positions.position_id",
                new EmployeeMapper());
    }

    public void update(Integer id, Employee object) {
        template.update("UPDATE employees SET employee_name=?,position_id=?,salary=?", object.getName(), object.getPosition().getId(), object.getSalary());
    }

    public void delete(Integer id) {
        template.update("DELETE FROM employees WHERE employee_id=?", id);
    }

    public List<Employee> findWithAvgOrHigherSalaryInPosition() {
        return template.query("SELECT employee_id, employee_name, position_id, salary, position_name FROM employees INNER JOIN " +
                        "(SELECT positions.position_id AS position_id_tmp, position_name, AVG(salary) AS average_salary " +
                        "FROM employees INNER JOIN positions ON employees.position_id=positions.position_id GROUP BY positions.position_id) " +
                        "WHERE position_id=position_id_tmp AND salary>=average_salary",
                new EmployeeMapper());
    }

    private class EmployeeMapper implements RowMapper<Employee> {

        public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
            return Employee.EmployeeBuilder
                    .basic()
                    .id(resultSet.getInt("employee_id"))
                    .name(resultSet.getString("employee_name"))
                    .position(new Position(resultSet.getInt("position_id"), resultSet.getString("position_name")))
                    .salary(resultSet.getInt("salary"))
                    .build();
        }
    }
}
