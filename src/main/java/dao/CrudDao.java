package dao;

import java.util.List;

public interface CrudDao<T> {
    void create(T object);

    T findById(Integer id);

    List<T> findAll();

    void update(Integer id, T object);

    void delete(Integer id);
}
