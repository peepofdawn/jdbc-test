package service;

import dao.EmployeeDao;
import dto.Employee;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class EmployeeService {
    @Autowired
    private EmployeeDao employeeDao;

    public void create(Employee object) {
        employeeDao.create(object);
    }

    public Employee findById(Integer id) {
        return employeeDao.findById(id);
    }

    public List<Employee> findAll() {
        return employeeDao.findAll();
    }

    public List<Employee> findWithAvgOrHigherSalaryInPosition() {
        return employeeDao.findWithAvgOrHigherSalaryInPosition();
    }

    public void updateById(Integer id, Employee object) {
        employeeDao.update(id, object);
    }

    public void delete(Integer id) {
        employeeDao.delete(id);
    }
}
