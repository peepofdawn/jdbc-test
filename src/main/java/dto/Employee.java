package dto;

public class Employee {
    private Integer id;
    private String name;
    private Position position;
    private Integer salary;

    public Employee() {

    }

    private Employee(EmployeeBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.position = builder.position;
        this.salary = builder.salary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public static class EmployeeBuilder {
        private Integer id;
        private String name;
        private Position position;
        private Integer salary;

        public static EmployeeBuilder basic() {
            return new EmployeeBuilder();
        }

        public EmployeeBuilder id(Integer id) {
            this.id = id;
            return this;
        }

        public EmployeeBuilder name(String name) {
            this.name = name;
            return this;
        }

        public EmployeeBuilder position(Position position) {
            this.position = position;
            return this;
        }

        public EmployeeBuilder salary(Integer salary) {
            this.salary = salary;
            return this;
        }

        public Employee build() {
            return new Employee(this);
        }
    }
}
