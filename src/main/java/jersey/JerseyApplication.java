package jersey;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/*")
public class JerseyApplication extends ResourceConfig {
    public JerseyApplication() {
        register(EmployeesResource.class);
    }
}
