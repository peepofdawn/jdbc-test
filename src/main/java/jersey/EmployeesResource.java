package jersey;

import dto.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import service.EmployeeService;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collection;

@Path("/employees")
public class EmployeesResource {

    @Autowired
    private EmployeeService employeeService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmployees() {
        Collection<Employee> employees = employeeService.findAll();
        return Response.ok().entity(employees).build();
    }

    @GET
    @Path("/averageorhighersalaryinposition")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmployeesWithHigherThanOrAveragePositionSalary() {
        Collection<Employee> employees = employeeService.findWithAvgOrHigherSalaryInPosition();
        return Response.ok().entity(employees).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmployeeById(@PathParam("id") Integer id) {
        Employee readEmployee = employeeService.findById(id);
        return Response.ok().entity(readEmployee).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateEmployee(@PathParam("id") Integer id, Employee employee){
        employeeService.updateById(id, employee);
        return Response.ok().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createEmployee(Employee employee, @Context UriInfo info){
        URI uriOfCreatedResource;
        employeeService.create(employee);
        uriOfCreatedResource = info.getBaseUriBuilder().path("/employees/" + employee.getId().toString()).build();
        return Response.created(uriOfCreatedResource).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCars(@PathParam("id") Integer id){
        employeeService.delete(id);
        return Response.ok().build();
    }
}
